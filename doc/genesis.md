## Genesis

```
git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"
```

Set up `.tool-versions` for an adequate version of Node.

Following instructions from 
https://nextjs.org/learn/basics/create-nextjs-app/setup .

```
npx create-next-app nextjs-blog --use-npm --example \
  "https://github.com/vercel/next-learn/tree/master/basics/learn-starter"
```
That created the example in a subdirectory. Integrated its contents up.

